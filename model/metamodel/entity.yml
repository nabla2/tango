entities:
  - name: entity
    description: エンティティ
    namespace: laplacian.metamodel
    properties:
      - name: name
        description: 名称
        constraint: pk
        type: string

      - name: namespace
        description: 名前空間
        type: string
        domain_type_name: namespace
        optional: true
        default_value: |
          if (inherited) {
              inheritedFrom.first().referenceEntity.namespace
          }
          else {
              _model.retrieve("project.namespace") ?: throw IllegalStateException(
                  "The ${name} entity does not have namespace." +
                  "You should give it or set the default(project) namespace instead."
              )
          }

      - name: identifier
        description: >
          識別子
          省略時は名称を使用
        type: string
        domain_type_name: identifier
        optional: true
        default_value: |
          name.lowerUnderscorize()

      - name: description
        description: 詳細
        optional: true
        type: string
        default_value: |
          name

      - name: value_object
        description: 値オブジェクトかどうか
        type: boolean
        optional: true
        default_value: |
          false

      - name: class_name
        description: クラス名
        type: string
        snippet: >
          identifier.upperCamelize()

      - name: inherited
        description: >
          他のエンティティの導出エンティティであるかどうか
        type: boolean
        snippet: >
          relationships.any{ it.inherited }

      - name: top_level
        description: >
          このエンティティがトップレベルエンティティかどうか
        type: boolean
        snippet: >
          !inherited && !valueObject

      - name: supports_namespace
        description: >
          このエンティティがnamespaceをサポートしているかどうか
        type: boolean
        snippet: |
          properties.any { p ->
              p.name == "namespace" && p.type == "string"
          }

      - name: fqn
        description: >
          完全修飾名
        type: string
        snippet: |
          "$namespace.$className"

      - name: primary_key_names
        description: |
          一意識別子となるカラム項目名のリスト
        type: string
        multiple: true
        snippet: |
          inheritedFrom.flatMap { inheritance ->
              inheritance.referenceEntity.primaryKeys.map { pk ->
                  "${inheritance.identifier.lowerUnderscorize()}_${pk.propertyName.lowerUnderscorize()}"
              }
          } + primaryKeys.map { it.propertyName.lowerUnderscorize() }

    relationships:
      - name: properties
        description: このエンティティのプロパティ
        reference_entity_name: property
        aggregate: true
        cardinality: 1..*

      - name: relationships
        description: このエンティティと他のエンティティの関連
        reference_entity_name: relationship
        aggregate: true
        cardinality: '*'

      - name: queries
        description: このエンティティに対するルートクエリ
        reference_entity_name: query
        aggregate: true
        cardinality: '*'

      - name: primary_keys
        description: 一意識別キーとなるプロパティのリスト
        reference_entity_name: property
        cardinality: '1..*'
        snippet: |
          properties.filter{ it.primaryKey }

      - name: inherited_from
        description: >
          このエンティティの導出元エンティティ
          このエンティティが導出エンティティでなければ空集合
        reference_entity_name: relationship
        cardinality: '*'
        snippet: >
          relationships.filter{ it.inherited }

      - name: relating_entities
        description: |
          このエンティティが参照するエンティティの一覧(自身は除く)
        reference_entity_name: entity
        cardinality: '*'
        snippet: |
          relationships
              .map{ it.referenceEntity }
              .filter{ it.fqn != this.fqn }
              .distinctBy{ it.fqn }

      - name: relating_top_level_entities
        description:
          このエンティティが参照するトップレベルエンティティの一覧(自身は除く)
        reference_entity_name: entity
        cardinality: '*'
        snippet: >
          relatingEntities.filter{ !it.inherited }

      - name: relating_external_entities
        description:
          このエンティティが参照する外部パッケージのエンティティ
        reference_entity_name: entity
        cardinality: '*'
        snippet: >
          relatingEntities.filter{ it.namespace != namespace }

      - name: aggregates
        description: このエンティティが管理する集約
        reference_entity_name: relationship
        cardinality: '*'
        snippet: >
          relationships.filter{ it.aggregate }

      - name: aggregated_entities
        description: |
          このエンティティに集約されているエンティティの一覧 (再帰的に集約されているものを含む)
        reference_entity_name: entity
        cardinality: '*'
        snippet: |
          (listOf(this) + aggregates.flatMap {
              it.referenceEntity.aggregatedEntities
          }).distinctBy{ it.fqn }

      - name: stored_properties
        description: このエンティティが直接値を保持するプロパティ
        reference_entity_name: property
        cardinality: '*'
        snippet: >
          properties.filter{ it.snippet == null }

      - name: stored_relationships
        description: このエンティティが直接関連値を保持している関連
        reference_entity_name: relationship
        cardinality: '*'
        snippet: >
          relationships.filter{ it.aggregate || it.mappings.isNotEmpty() }

    queries:
      - name: top_level
        result_entity_name: entity
        cardinality: '*'
        description: >
          トップレベルエンティティの一覧
        snippet: |
          return filter{ it.topLevel }

      - name: top_level_in_namespace
        result_entity_name: entity
        cardinality: '*'
        description: |
          The top level entities which are included in the same namespace.
        snippet: |
          return inNamespace.filter{ it.topLevel }

