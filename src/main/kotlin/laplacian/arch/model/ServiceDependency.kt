package laplacian.arch.model
import laplacian.arch.service.model.Service
import laplacian.util.*
/**
 * service_dependency
 */
interface ServiceDependency {
    /**
     * The service_name of this service_dependency.
     */
    val serviceName: String
    /**
     * client
     */
    val client: Client
    /**
     * service
     */
    val service: Service
}