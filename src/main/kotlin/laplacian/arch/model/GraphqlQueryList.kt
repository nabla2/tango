package laplacian.arch.model
import laplacian.metamodel.model.Entity
import laplacian.util.*
/**
 * A container for records of graphql_query
 */
class GraphqlQueryList(
    list: List<GraphqlQuery>,
    val model: Model
) : List<GraphqlQuery> by list {
}