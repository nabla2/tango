package laplacian.arch.model
import laplacian.arch.client.pwa.model.Feature
import laplacian.arch.service.model.Service
import laplacian.util.*
/**
 * A container for records of client
 */
class ClientList(
    list: List<Client>,
    val model: Model
) : List<Client> by list {
}