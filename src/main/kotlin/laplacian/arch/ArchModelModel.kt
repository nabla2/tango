package laplacian.arch
import laplacian.util.*
import laplacian.arch.datasource.model.DatasourceList
import laplacian.arch.datasource.record.DatasourceRecord
import laplacian.util.*
import laplacian.arch.service.model.ServiceList
import laplacian.arch.service.record.ServiceRecord
import laplacian.util.*
import laplacian.arch.service.rest.model.RestResourceList
import laplacian.arch.service.rest.record.RestResourceRecord
import laplacian.util.*
import laplacian.arch.service.rest.model.DataAccessList
import laplacian.arch.service.rest.record.DataAccessRecord
import laplacian.util.*
import laplacian.arch.client.pwa.model.PageList
import laplacian.arch.client.pwa.record.PageRecord
import laplacian.util.*
import laplacian.arch.model.ClientList
import laplacian.arch.record.ClientRecord
import laplacian.util.*
import laplacian.arch.client.pwa.model.WidgetList
import laplacian.arch.client.pwa.record.WidgetRecord
import laplacian.util.*
import laplacian.arch.client.pwa.model.FeatureList
import laplacian.arch.client.pwa.record.FeatureRecord
import laplacian.util.*
import laplacian.metamodel.model.EntityList
import laplacian.metamodel.record.EntityRecord
import laplacian.util.*
import laplacian.metamodel.model.ValueDomainTypeList
import laplacian.metamodel.record.ValueDomainTypeRecord
class ArchModelModel(model: Model): Map<String, Any?> by model {
    val datasources: DatasourceList = DatasourceRecord.from(model)
    val services: ServiceList = ServiceRecord.from(model)
    val rest_resources: RestResourceList = RestResourceRecord.from(model)
    val data_accesses: DataAccessList = DataAccessRecord.from(model)
    val pages: PageList = PageRecord.from(model)
    val clients: ClientList = ClientRecord.from(model)
    val widgets: WidgetList = WidgetRecord.from(model)
    val features: FeatureList = FeatureRecord.from(model)
    val entities: EntityList = EntityRecord.from(model)
    val value_domain_types: ValueDomainTypeList = ValueDomainTypeRecord.from(model)
}