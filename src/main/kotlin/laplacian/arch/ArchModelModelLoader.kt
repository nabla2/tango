package laplacian.arch
import laplacian.util.*
import laplacian.ModelLoader
import java.io.File
/**
 * A loader class for ArchModelModel.
 */
class ArchModelModelLoader : ModelLoader<ArchModelModel> {
    /**
     * load a model object from the given yaml files.
     */
    override fun load(files: Iterable<File>): ArchModelModel {
        val model: Model = YamlLoader.readObjects(files)
        return ArchModelModel(model)
    }
}