package laplacian.arch.datasource.record
import laplacian.arch.datasource.model.EntityReference
import laplacian.arch.datasource.model.Datasource
import laplacian.metamodel.model.Entity
import laplacian.metamodel.record.EntityRecord
import laplacian.util.*
/**
 * entity_reference
 */
data class EntityReferenceRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the datasource which aggregates this entity_reference
     */
    override val datasource: Datasource
): EntityReference, Record by _record {
    /**
     * The entity_name of this entity_reference.
     */
    override val entityName: String
        get() = getOrThrow("entityName")
    /**
     * entity
     */
    override val entity: Entity
        get() = EntityRecord.from(_model).find {
            it.name == entityName
        } ?: throw IllegalStateException(
            "There is no entity which meets the following condition(s): "
            + "EntityReference.entity_name == entity.name (=$entityName) "
            + "Possible values are: " + EntityRecord.from(_model).map {
              "(${ it.name })"
            }.joinToString()
        )
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, datasource: Datasource) = records.map {
            EntityReferenceRecord(it.normalizeCamelcase(), model, datasource = datasource)
        }
    }
}