package laplacian.arch.datasource.model
import laplacian.metamodel.model.Entity
import laplacian.util.*
/**
 * A container for records of datasource
 */
class DatasourceList(
    list: List<Datasource>,
    val model: Model
) : List<Datasource> by list {
}