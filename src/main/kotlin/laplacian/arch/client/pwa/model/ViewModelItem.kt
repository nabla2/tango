package laplacian.arch.client.pwa.model
import laplacian.metamodel.model.Entity
import laplacian.util.*
/**
 * view_model_item
 */
interface ViewModelItem {
    /**
     * 項目名
     */
    val name: String
    /**
     * The identifier of this view_model_item.
     */
    val identifier: String
    /**
     * 詳細
     */
    val description: String
    /**
     * define wether this item has multiple values or not
     */
    val multiple: Boolean
    /**
     * この項目が参照するエンティティ名(typeとは排他利用)
     */
    val entityName: String?
    /**
     * この項目のデータ型
     */
    val type: String
    /**
     * この項目の初期値
     */
    val defaultValue: String?
    /**
     * この項目が参照するエンティティ(単純型の場合はnull)
     */
    val entity: Entity?
    /**
     * feature
     */
    val feature: Feature?
    /**
     * 子要素
     */
    val children: List<ViewModelItem>
    /**
     * 親要素(トップレベル要素の場合はnull)
     */
    val parent: ViewModelItem?
}