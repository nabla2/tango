package laplacian.arch.client.pwa.model
import laplacian.util.*
/**
 * page_state
 */
interface PageState {
    /**
     * The name of this page_state.
     */
    val name: String
    /**
     * The type of this page_state.
     */
    val type: String
    /**
     * The expression of this page_state.
     */
    val expression: String
    /**
     * page
     */
    val page: Page
}