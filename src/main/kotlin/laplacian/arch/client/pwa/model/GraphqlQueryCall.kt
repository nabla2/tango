package laplacian.arch.client.pwa.model
import laplacian.arch.service.model.Service
import laplacian.arch.service.model.GraphqlQuery
import laplacian.util.*
/**
 * graphql_query_call
 */
interface GraphqlQueryCall {
    /**
     * The service_name of this graphql_query_call.
     */
    val serviceName: String
    /**
     * The name of this graphql_query_call.
     */
    val name: String
    /**
     * action
     */
    val action: PageAction
    /**
     * service
     */
    val service: Service
    /**
     * graphql_query
     */
    val graphqlQuery: GraphqlQuery
        get() = service.graphqlQueries.find{ it.name == name }!!
}