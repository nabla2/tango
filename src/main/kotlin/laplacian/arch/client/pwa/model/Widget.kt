package laplacian.arch.client.pwa.model
import laplacian.metamodel.model.NamedParam
import laplacian.util.*
/**
 * UI部品
 */
interface Widget {
    /**
     * グループ
     */
    val group: String
    /**
     * 名称
     */
    val name: String
    /**
     * HTMLテンプレート
     */
    val template: String
    /**
     * このUI部品が外部に公開するパラメータ
     */
    val params: List<NamedParam>
    /**
     * このUI部品が外部に公開するイベント
     */
    val events: List<NamedParam>
}