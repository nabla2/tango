package laplacian.arch.client.pwa.model
import laplacian.metamodel.model.NamedParam
import laplacian.util.*
/**
 * ページ内アクション定義
 */
interface PageAction {
    /**
     * The name of this page_action.
     */
    val name: String
    /**
     * The identifier of this page_action.
     */
    val identifier: String
    /**
     * The description of this page_action.
     */
    val description: String
    /**
     * page
     */
    val page: Page
    /**
     * params
     */
    val params: List<NamedParam>
    /**
     * rest_api_call
     */
    val restApiCall: RestApiCall?
    /**
     * graphql_query_call
     */
    val graphqlQueryCall: GraphqlQueryCall?
    /**
     * examples
     */
    val examples: List<PageActionExample>
}