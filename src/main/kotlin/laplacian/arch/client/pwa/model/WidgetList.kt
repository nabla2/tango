package laplacian.arch.client.pwa.model
import laplacian.metamodel.model.NamedParam
import laplacian.util.*
/**
 * A container for records of widget
 */
class WidgetList(
    list: List<Widget>,
    val model: Model
) : List<Widget> by list {
}