package laplacian.arch.client.pwa.model
import laplacian.metamodel.model.Entity
import laplacian.util.*
/**
 * A container for records of feature
 */
class FeatureList(
    list: List<Feature>,
    val model: Model
) : List<Feature> by list {
    /**
     * relating_entities
     */
    val relatingEntities: List<Entity>
        get() = flatMap{ it.relatingEntities }.distinctBy{ it.fqn }
}