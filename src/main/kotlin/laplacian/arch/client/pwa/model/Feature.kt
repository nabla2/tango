package laplacian.arch.client.pwa.model
import laplacian.metamodel.model.Entity
import laplacian.util.*
/**
 * 機能
 */
interface Feature {
    /**
     * 名称
     */
    val name: String
    /**
     * 識別子
     */
    val identifier: String
    /**
     * ベースパス
     */
    val basePath: String
    /**
     * 初期遷移先
     */
    val initial: Boolean
    /**
     * 画面一覧
     */
    val pages: List<Page>
    /**
     * 状態
     */
    val state: List<ViewModelItem>
    /**
     * 状態クエリ
     */
    val stateQueries: List<ViewModelOperation>
    /**
     * 状態更新
     */
    val stateMutation: List<ViewModelOperation>
    /**
     * The list of entities this feature refers to
     */
    val relatingEntities: List<Entity>
        get() = (state.map{ it.entity } + stateQueries.map{ it.entity })
        .filterNotNull()
        .distinctBy{ it.fqn }
}