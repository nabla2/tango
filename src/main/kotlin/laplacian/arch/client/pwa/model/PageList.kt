package laplacian.arch.client.pwa.model
import laplacian.metamodel.model.NamedValue
import laplacian.util.*
/**
 * A container for records of page
 */
class PageList(
    list: List<Page>,
    val model: Model
) : List<Page> by list {
}