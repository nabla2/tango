package laplacian.arch.client.pwa.model
import laplacian.metamodel.model.NamedValue
import laplacian.util.*
/**
 * 画面
 */
interface Page {
    /**
     * 名称
     */
    val name: String
    /**
     * 機能名
     */
    val featureName: String
    /**
     * 識別子
     */
    val identifier: String
    /**
     * パス
     */
    val path: String?
    /**
     * 初期画面
     */
    val initial: Boolean
    /**
     * モーダル画面
     */
    val modal: Boolean
    /**
     * 画面描画事前条件
     */
    val renderPageWhen: String?
    /**
     * 機能
     */
    val feature: Feature
    /**
     * 画面項目リスト
     */
    val items: List<PageItem>
    /**
     * 画面アクションリスト
     */
    val actions: List<PageAction>
    /**
     * 画面ステート
     */
    val states: List<PageState>
    /**
     * イベント
     */
    val events: List<NamedValue>
}