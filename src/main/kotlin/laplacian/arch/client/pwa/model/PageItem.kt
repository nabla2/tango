package laplacian.arch.client.pwa.model
import laplacian.metamodel.model.NamedValue
import laplacian.util.*
/**
 * 画面項目
 */
interface PageItem {
    /**
     * 名称
     */
    val name: String
    /**
     * 識別子
     */
    val identifier: String
    /**
     * The widget_group of this page_item.
     */
    val widgetGroup: String
    /**
     * The widget_name of this page_item.
     */
    val widgetName: String
    /**
     * 画面(最上位項目でない場合はnull)
     */
    val page: Page?
    /**
     * widget
     */
    val widget: Widget
    /**
     * widget_params
     */
    val widgetParams: List<NamedValue>
    /**
     * events
     */
    val events: List<NamedValue>
    /**
     * 子要素
     */
    val children: List<PageItem>
    /**
     * 親要素(トップレベル要素の場合はnull)
     */
    val parent: PageItem?
}