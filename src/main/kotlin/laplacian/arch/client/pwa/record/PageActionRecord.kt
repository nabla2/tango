package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.PageAction
import laplacian.arch.client.pwa.model.Page
import laplacian.metamodel.model.NamedParam
import laplacian.metamodel.record.NamedParamRecord
import laplacian.arch.client.pwa.model.RestApiCall
import laplacian.arch.client.pwa.model.GraphqlQueryCall
import laplacian.arch.client.pwa.model.PageActionExample
import laplacian.util.*
/**
 * ページ内アクション定義
 */
data class PageActionRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the page which aggregates this page_action
     */
    override val page: Page
): PageAction, Record by _record {
    /**
     * The name of this page_action.
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * The identifier of this page_action.
     */
    override val identifier: String
        get() = getOrThrow("identifier") {
            name.lowerHyphenize()
        }
    /**
     * The description of this page_action.
     */
    override val description: String
        get() = getOrThrow("description") {
            "The ${name} action in ${page.name}"
        }
    /**
     * params
     */
    override val params: List<NamedParam>
        = NamedParamRecord.from(getList("params", emptyList()), _model)
    /**
     * rest_api_call
     */
    override val restApiCall: RestApiCall?
        = getOrNull<Record>("rest_api_call")?.let{ RestApiCallRecord(it.normalizeCamelcase(), _model, this) }
    /**
     * graphql_query_call
     */
    override val graphqlQueryCall: GraphqlQueryCall?
        = getOrNull<Record>("graphql_query_call")?.let{ GraphqlQueryCallRecord(it.normalizeCamelcase(), _model, this) }
    /**
     * examples
     */
    override val examples: List<PageActionExample>
        = PageActionExampleRecord.from(getList("examples", emptyList()), _model, this)
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, page: Page) = records.map {
            PageActionRecord(it.normalizeCamelcase(), model, page = page)
        }
    }
}