package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.PageItem
import laplacian.arch.client.pwa.model.Page
import laplacian.arch.client.pwa.model.Widget
import laplacian.metamodel.model.NamedValue
import laplacian.metamodel.record.NamedValueRecord
import laplacian.util.*
/**
 * 画面項目
 */
data class PageItemRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the page which aggregates this page_item
     */
    override val page: Page? = null,
    /**
     * the parent which aggregates this page_item
     */
    override val parent: PageItem? = null
): PageItem, Record by _record {
    /**
     * 名称
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * 識別子
     */
    override val identifier: String
        get() = getOrThrow("identifier") {
            name.lowerUnderscorize()
        }
    /**
     * The widget_group of this page_item.
     */
    override val widgetGroup: String
        get() = getOrThrow("widgetGroup")
    /**
     * The widget_name of this page_item.
     */
    override val widgetName: String
        get() = getOrThrow("widgetName")
    /**
     * widget
     */
    override val widget: Widget
        get() = WidgetRecord.from(_model).find {
            it.group == widgetGroup &&
            it.name == widgetName
        } ?: throw IllegalStateException(
            "There is no widget which meets the following condition(s): "
            + "PageItem.widget_group == widget.group (=$widgetGroup) "
            + "PageItem.widget_name == widget.name (=$widgetName) "
            + "Possible values are: " + WidgetRecord.from(_model).map {
              "(${ it.group },${ it.name })"
            }.joinToString()
        )
    /**
     * widget_params
     */
    override val widgetParams: List<NamedValue>
        = NamedValueRecord.from(getList("widget_params", emptyList()), _model)
    /**
     * events
     */
    override val events: List<NamedValue>
        = NamedValueRecord.from(getList("events", emptyList()), _model)
    /**
     * 子要素
     */
    override val children: List<PageItem>
        = PageItemRecord.from(getList("children", emptyList()), _model, this)
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, page: Page? = null) = records.map {
            PageItemRecord(it.normalizeCamelcase(), model, page = page)
        }
        fun from(records: RecordList, model: Model, parent: PageItem? = null) = records.map {
            PageItemRecord(it.normalizeCamelcase(), model, parent = parent)
        }
    }
}