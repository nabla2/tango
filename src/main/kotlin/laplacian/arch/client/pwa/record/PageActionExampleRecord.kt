package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.PageActionExample
import laplacian.arch.client.pwa.model.PageAction
import laplacian.util.*
/**
 * page_action_example
 */
data class PageActionExampleRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the action which aggregates this page_action_example
     */
    override val action: PageAction
): PageActionExample, Record by _record {
    /**
     * The title of this page_action_example.
     */
    override val title: String
        get() = getOrThrow("title")
    /**
     * The given_params of this page_action_example.
     */
    override val givenParams: String? by _record
    /**
     * The given_state of this page_action_example.
     */
    override val givenState: String? by _record
    /**
     * The then of this page_action_example.
     */
    override val then: String
        get() = getOrThrow("then")
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, action: PageAction) = records.map {
            PageActionExampleRecord(it.normalizeCamelcase(), model, action = action)
        }
    }
}