package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.Widget
import laplacian.arch.client.pwa.model.WidgetList
import laplacian.metamodel.model.NamedParam
import laplacian.metamodel.record.NamedParamRecord
import laplacian.util.*
/**
 * UI部品
 */
data class WidgetRecord (
    private val _record: Record,
    private val _model: Model
): Widget, Record by _record {
    /**
     * グループ
     */
    override val group: String
        get() = getOrThrow("group")
    /**
     * 名称
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * HTMLテンプレート
     */
    override val template: String
        get() = getOrThrow("template")
    /**
     * このUI部品が外部に公開するパラメータ
     */
    override val params: List<NamedParam>
        = NamedParamRecord.from(getList("params", emptyList()), _model)
    /**
     * このUI部品が外部に公開するイベント
     */
    override val events: List<NamedParam>
        = NamedParamRecord.from(getList("events", emptyList()), _model)
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(model: Model): WidgetList {
            val entities = model.getList<Record>("widgets", emptyList()).map {
                WidgetRecord(it.normalizeCamelcase(), model)
            }
            return WidgetList(entities, model)
        }
    }
}