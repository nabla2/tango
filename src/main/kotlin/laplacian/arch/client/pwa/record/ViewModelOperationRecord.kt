package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.ViewModelOperation
import laplacian.arch.client.pwa.model.Feature
import laplacian.metamodel.model.NamedParam
import laplacian.metamodel.record.NamedParamRecord
import laplacian.metamodel.model.Entity
import laplacian.metamodel.record.EntityRecord
import laplacian.util.*
/**
 * view_model_operation
 */
data class ViewModelOperationRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the feature which aggregates this view_model_operation
     */
    override val feature: Feature
): ViewModelOperation, Record by _record {
    /**
     * The name of this view_model_operation.
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * The description of this view_model_operation.
     */
    override val description: String? by _record
    /**
     * The return_type of this view_model_operation.
     */
    override val returnType: String
        get() = getOrThrow("returnType") {
            entity?.let{ it.className + if (multiple) "[]" else "" }
        }
    /**
     * Defines this view_model_operation is multiple or not.
     */
    override val multiple: Boolean
        get() = getOrThrow("multiple") {
            false
        }
    /**
     * The return_entity_name of this view_model_operation.
     */
    override val returnEntityName: String? by _record
    /**
     * The snippet of this view_model_operation.
     */
    override val snippet: String
        get() = getOrThrow("snippet")
    /**
     * params
     */
    override val params: List<NamedParam>
        = NamedParamRecord.from(getList("params", emptyList()), _model)
    /**
     * entity
     */
    override val entity: Entity?
        get() = EntityRecord.from(_model).find {
            it.name == returnEntityName
        }
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, feature: Feature) = records.map {
            ViewModelOperationRecord(it.normalizeCamelcase(), model, feature = feature)
        }
    }
}