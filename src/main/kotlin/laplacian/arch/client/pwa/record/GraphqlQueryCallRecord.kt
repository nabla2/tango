package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.GraphqlQueryCall
import laplacian.arch.client.pwa.model.PageAction
import laplacian.arch.service.model.Service
import laplacian.arch.service.record.ServiceRecord
import laplacian.arch.service.model.GraphqlQuery
import laplacian.arch.service.record.GraphqlQueryRecord
import laplacian.util.*
/**
 * graphql_query_call
 */
data class GraphqlQueryCallRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the action which aggregates this graphql_query_call
     */
    override val action: PageAction
): GraphqlQueryCall, Record by _record {
    /**
     * The service_name of this graphql_query_call.
     */
    override val serviceName: String
        get() = getOrThrow("serviceName")
    /**
     * The name of this graphql_query_call.
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * service
     */
    override val service: Service
        get() = ServiceRecord.from(_model).find {
            it.name == serviceName
        } ?: throw IllegalStateException(
            "There is no service which meets the following condition(s): "
            + "GraphqlQueryCall.service_name == service.name (=$serviceName) "
            + "Possible values are: " + ServiceRecord.from(_model).map {
              "(${ it.name })"
            }.joinToString()
        )
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, action: PageAction) = records.map {
            GraphqlQueryCallRecord(it.normalizeCamelcase(), model, action = action)
        }
    }
}