package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.ViewModelItem
import laplacian.metamodel.model.Entity
import laplacian.metamodel.record.EntityRecord
import laplacian.arch.client.pwa.model.Feature
import laplacian.util.*
/**
 * view_model_item
 */
data class ViewModelItemRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the feature which aggregates this view_model_item
     */
    override val feature: Feature? = null,
    /**
     * the parent which aggregates this view_model_item
     */
    override val parent: ViewModelItem? = null
): ViewModelItem, Record by _record {
    /**
     * 項目名
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * The identifier of this view_model_item.
     */
    override val identifier: String
        get() = getOrThrow("identifier") {
            name.lowerUnderscorize()
        }
    /**
     * 詳細
     */
    override val description: String
        get() = getOrThrow("description") {
            name
        }
    /**
     * define wether this item has multiple values or not
     */
    override val multiple: Boolean
        get() = getOrThrow("multiple") {
            false
        }
    /**
     * この項目が参照するエンティティ名(typeとは排他利用)
     */
    override val entityName: String? by _record
    /**
     * この項目のデータ型
     */
    override val type: String
        get() = getOrThrow("type") {
            entity?.let{ it.className + if (multiple) "[]" else "" }
        }
    /**
     * この項目の初期値
     */
    override val defaultValue: String? by _record
    /**
     * この項目が参照するエンティティ(単純型の場合はnull)
     */
    override val entity: Entity?
        get() = EntityRecord.from(_model).find {
            it.name == entityName
        }
    /**
     * 子要素
     */
    override val children: List<ViewModelItem>
        = ViewModelItemRecord.from(getList("children", emptyList()), _model, this)
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, feature: Feature? = null) = records.map {
            ViewModelItemRecord(it.normalizeCamelcase(), model, feature = feature)
        }
        fun from(records: RecordList, model: Model, parent: ViewModelItem? = null) = records.map {
            ViewModelItemRecord(it.normalizeCamelcase(), model, parent = parent)
        }
    }
}