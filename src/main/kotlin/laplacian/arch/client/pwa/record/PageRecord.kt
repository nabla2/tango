package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.Page
import laplacian.arch.client.pwa.model.PageList
import laplacian.arch.client.pwa.model.Feature
import laplacian.arch.client.pwa.model.PageItem
import laplacian.arch.client.pwa.model.PageAction
import laplacian.arch.client.pwa.model.PageState
import laplacian.metamodel.model.NamedValue
import laplacian.metamodel.record.NamedValueRecord
import laplacian.util.*
/**
 * 画面
 */
data class PageRecord (
    private val _record: Record,
    private val _model: Model
): Page, Record by _record {
    /**
     * 名称
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * 機能名
     */
    override val featureName: String
        get() = getOrThrow("featureName")
    /**
     * 識別子
     */
    override val identifier: String
        get() = getOrThrow("identifier") {
            name.lowerUnderscorize()
        }
    /**
     * パス
     */
    override val path: String? by _record
    /**
     * 初期画面
     */
    override val initial: Boolean
        get() = getOrThrow("initial") {
            false
        }
    /**
     * モーダル画面
     */
    override val modal: Boolean
        get() = getOrThrow("modal") {
            false
        }
    /**
     * 画面描画事前条件
     */
    override val renderPageWhen: String? by _record
    /**
     * 機能
     */
    override val feature: Feature
        get() = FeatureRecord.from(_model).find {
            it.name == featureName
        } ?: throw IllegalStateException(
            "There is no feature which meets the following condition(s): "
            + "Page.feature_name == feature.name (=$featureName) "
            + "Possible values are: " + FeatureRecord.from(_model).map {
              "(${ it.name })"
            }.joinToString()
        )
    /**
     * 画面項目リスト
     */
    override val items: List<PageItem>
        = PageItemRecord.from(getList("items", emptyList()), _model, this)
    /**
     * 画面アクションリスト
     */
    override val actions: List<PageAction>
        = PageActionRecord.from(getList("actions", emptyList()), _model, this)
    /**
     * 画面ステート
     */
    override val states: List<PageState>
        = PageStateRecord.from(getList("states", emptyList()), _model, this)
    /**
     * イベント
     */
    override val events: List<NamedValue>
        = NamedValueRecord.from(getList("events", emptyList()), _model)
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(model: Model): PageList {
            val entities = model.getList<Record>("pages", emptyList()).map {
                PageRecord(it.normalizeCamelcase(), model)
            }
            return PageList(entities, model)
        }
    }
}