package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.PageState
import laplacian.arch.client.pwa.model.Page
import laplacian.util.*
/**
 * page_state
 */
data class PageStateRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the page which aggregates this page_state
     */
    override val page: Page
): PageState, Record by _record {
    /**
     * The name of this page_state.
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * The type of this page_state.
     */
    override val type: String
        get() = getOrThrow("type")
    /**
     * The expression of this page_state.
     */
    override val expression: String
        get() = getOrThrow("expression")
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, page: Page) = records.map {
            PageStateRecord(it.normalizeCamelcase(), model, page = page)
        }
    }
}