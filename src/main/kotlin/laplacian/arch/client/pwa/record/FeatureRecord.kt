package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.Feature
import laplacian.arch.client.pwa.model.FeatureList
import laplacian.arch.client.pwa.model.Page
import laplacian.arch.client.pwa.model.ViewModelItem
import laplacian.arch.client.pwa.model.ViewModelOperation
import laplacian.metamodel.model.Entity
import laplacian.metamodel.record.EntityRecord
import laplacian.util.*
/**
 * 機能
 */
data class FeatureRecord (
    private val _record: Record,
    private val _model: Model
): Feature, Record by _record {
    /**
     * 名称
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * 識別子
     */
    override val identifier: String
        get() = getOrThrow("identifier") {
            name.lowerUnderscorize()
        }
    /**
     * ベースパス
     */
    override val basePath: String
        get() = getOrThrow("basePath") {
            identifier
        }
    /**
     * 初期遷移先
     */
    override val initial: Boolean
        get() = getOrThrow("initial") {
            false
        }
    /**
     * 画面一覧
     */
    override val pages: List<Page>
        get() = PageRecord.from(_model).filter {
            it.featureName == name
        }
    /**
     * 状態
     */
    override val state: List<ViewModelItem>
        = ViewModelItemRecord.from(getList("state", emptyList()), _model, this)
    /**
     * 状態クエリ
     */
    override val stateQueries: List<ViewModelOperation>
        = ViewModelOperationRecord.from(getList("state_queries", emptyList()), _model, this)
    /**
     * 状態更新
     */
    override val stateMutation: List<ViewModelOperation>
        = ViewModelOperationRecord.from(getList("state_mutation", emptyList()), _model, this)
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(model: Model): FeatureList {
            val entities = model.getList<Record>("features", emptyList()).map {
                FeatureRecord(it.normalizeCamelcase(), model)
            }
            return FeatureList(entities, model)
        }
    }
}