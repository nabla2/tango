package laplacian.arch.client.pwa.record
import laplacian.arch.client.pwa.model.RestApiCall
import laplacian.arch.client.pwa.model.PageAction
import laplacian.arch.service.model.Service
import laplacian.arch.service.record.ServiceRecord
import laplacian.arch.service.rest.model.RestResource
import laplacian.arch.service.rest.record.RestResourceRecord
import laplacian.arch.service.rest.model.RestOperation
import laplacian.arch.service.rest.record.RestOperationRecord
import laplacian.util.*
/**
 * rest_api_call
 */
data class RestApiCallRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the action which aggregates this rest_api_call
     */
    override val action: PageAction
): RestApiCall, Record by _record {
    /**
     * The service_name of this rest_api_call.
     */
    override val serviceName: String
        get() = getOrThrow("serviceName")
    /**
     * The resource_name of this rest_api_call.
     */
    override val resourceName: String
        get() = getOrThrow("resourceName")
    /**
     * The http_method of this rest_api_call.
     */
    override val httpMethod: String
        get() = getOrThrow("httpMethod")
    /**
     * The path of this rest_api_call.
     */
    override val path: String
        get() = getOrThrow("path") {
            "/"
        }
    /**
     * service
     */
    override val service: Service
        get() = ServiceRecord.from(_model).find {
            it.name == serviceName
        } ?: throw IllegalStateException(
            "There is no service which meets the following condition(s): "
            + "RestApiCall.service_name == service.name (=$serviceName) "
            + "Possible values are: " + ServiceRecord.from(_model).map {
              "(${ it.name })"
            }.joinToString()
        )
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, action: PageAction) = records.map {
            RestApiCallRecord(it.normalizeCamelcase(), model, action = action)
        }
    }
}