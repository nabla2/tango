package laplacian.arch.service.model
import laplacian.arch.service.rest.model.RestResource
import laplacian.arch.datasource.model.Datasource
import laplacian.metamodel.model.Entity
import laplacian.util.*
/**
 * A container for records of service
 */
class ServiceList(
    list: List<Service>,
    val model: Model
) : List<Service> by list {
    val inNamespace: List<Service>
        get() = filter {
            it.namespace.startsWith(model.retrieve<String>("project.namespace")!!)
        }
}