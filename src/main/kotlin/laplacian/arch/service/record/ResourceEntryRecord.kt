package laplacian.arch.service.record
import laplacian.arch.service.model.ResourceEntry
import laplacian.arch.service.model.Service
import laplacian.arch.service.rest.model.RestResource
import laplacian.arch.service.rest.record.RestResourceRecord
import laplacian.util.*
/**
 * resource_entry
 */
data class ResourceEntryRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the service which aggregates this resource_entry
     */
    override val service: Service
): ResourceEntry, Record by _record {
    /**
     * The resource_name of this resource_entry.
     */
    override val resourceName: String
        get() = getOrThrow("resourceName")
    /**
     * resource
     */
    override val resource: RestResource
        get() = RestResourceRecord.from(_model).find {
            it.name == resourceName
        } ?: throw IllegalStateException(
            "There is no rest_resource which meets the following condition(s): "
            + "ResourceEntry.resource_name == rest_resource.name (=$resourceName) "
            + "Possible values are: " + RestResourceRecord.from(_model).map {
              "(${ it.name })"
            }.joinToString()
        )
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, service: Service) = records.map {
            ResourceEntryRecord(it.normalizeCamelcase(), model, service = service)
        }
    }
}