package laplacian.arch.service.rest.record
import laplacian.arch.service.rest.model.RestDataItem
import laplacian.arch.service.rest.model.RestOperation
import laplacian.metamodel.model.Entity
import laplacian.metamodel.record.EntityRecord
import laplacian.util.*
/**
 * rest_data_item
 */
data class RestDataItemRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the operation which aggregates this rest_data_item
     */
    override val operation: RestOperation? = null,
    /**
     * the parent which aggregates this rest_data_item
     */
    override val parent: RestDataItem? = null
): RestDataItem, Record by _record {
    /**
     * The name of this rest_data_item.
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * The identifier of this rest_data_item.
     */
    override val identifier: String
        get() = getOrThrow("identifier") {
            name.lowerUnderscorize()
        }
    /**
     * The type of this rest_data_item.
     */
    override val type: String? by _record
    /**
     * The entity_name of this rest_data_item.
     */
    override val entityName: String? by _record
    /**
     * The description of this rest_data_item.
     */
    override val description: String
        get() = getOrThrow("description") {
            name
        }
    /**
     * Defines this rest_data_item is root or not.
     */
    override val root: Boolean
        get() = getOrThrow("root") {
            false
        }
    /**
     * Defines this rest_data_item is multiple or not.
     */
    override val multiple: Boolean
        get() = getOrThrow("multiple") {
            false
        }
    /**
     * Defines this rest_data_item is required or not.
     */
    override val required: Boolean
        get() = getOrThrow("required") {
            false
        }
    /**
     * The default_value of this rest_data_item.
     */
    override val defaultValue: String? by _record
    /**
     * The example_value of this rest_data_item.
     */
    override val exampleValue: String
        get() = getOrThrow("exampleValue")
    /**
     * children
     */
    override val children: List<RestDataItem>
        = RestDataItemRecord.from(getList("children", emptyList()), _model, this)
    /**
     * entity
     */
    override val entity: Entity?
        get() = EntityRecord.from(_model).find {
            it.name == entityName
        }
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, operation: RestOperation? = null) = records.map {
            RestDataItemRecord(it.normalizeCamelcase(), model, operation = operation)
        }
        fun from(records: RecordList, model: Model, parent: RestDataItem? = null) = records.map {
            RestDataItemRecord(it.normalizeCamelcase(), model, parent = parent)
        }
    }
}