package laplacian.arch.service.rest.record
import laplacian.arch.service.rest.model.RestOperation
import laplacian.arch.service.rest.model.RestResource
import laplacian.metamodel.model.Entity
import laplacian.metamodel.record.EntityRecord
import laplacian.arch.service.rest.model.RestDataItem
import laplacian.util.*
/**
 * rest_operation
 */
data class RestOperationRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the resource which aggregates this rest_operation
     */
    override val resource: RestResource
): RestOperation, Record by _record {
    /**
     * The method of this rest_operation.
     */
    override val method: String
        get() = getOrThrow("method")
    /**
     * The path of this rest_operation.
     */
    override val path: String
        get() = getOrThrow("path") {
            "/"
        }
    /**
     * The name of this rest_operation.
     */
    override val name: String
        get() = getOrThrow("name") {
            identifier
        }
    /**
     * The identifier of this rest_operation.
     */
    override val identifier: String
        get() = getOrThrow("identifier") {
            method.lowerUnderscorize() + "_" +
            if (path == "/") {
                resource.identifier.pluralize()
            }
            else {
                path.lowerUnderscorize()
            }
        }
    /**
     * The namespace of this rest_operation.
     */
    override val namespace: String
        get() = getOrThrow("namespace") {
            "${resource.namespace}.$identifier"
        }
    /**
     * The description of this rest_operation.
     */
    override val description: String
        get() = getOrThrow("description") {
            name
        }
    /**
     * path_parameters
     */
    override val pathParameters: List<RestDataItem>
        = RestDataItemRecord.from(getList("path_parameters", emptyList()), _model, this)
    /**
     * request_headers
     */
    override val requestHeaders: List<RestDataItem>
        = RestDataItemRecord.from(getList("request_headers", emptyList()), _model, this)
    /**
     * query_parameters
     */
    override val queryParameters: List<RestDataItem>
        = RestDataItemRecord.from(getList("query_parameters", emptyList()), _model, this)
    /**
     * request_body
     */
    override val requestBody: List<RestDataItem>
        = RestDataItemRecord.from(getList("request_body", emptyList()), _model, this)
    /**
     * response_headers
     */
    override val responseHeaders: List<RestDataItem>
        = RestDataItemRecord.from(getList("response_headers", emptyList()), _model, this)
    /**
     * response_body
     */
    override val responseBody: List<RestDataItem>
        = RestDataItemRecord.from(getList("response_body", emptyList()), _model, this)
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, resource: RestResource) = records.map {
            RestOperationRecord(it.normalizeCamelcase(), model, resource = resource)
        }
    }
}