package laplacian.arch.service.rest.record
import laplacian.arch.service.rest.model.RestResource
import laplacian.arch.service.rest.model.RestResourceList
import laplacian.arch.service.rest.model.RestOperation
import laplacian.metamodel.model.Entity
import laplacian.metamodel.record.EntityRecord
import laplacian.util.*
/**
 * rest_resource
 */
data class RestResourceRecord (
    private val _record: Record,
    private val _model: Model
): RestResource, Record by _record {
    /**
     * The name of this rest_resource.
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * The identifier of this rest_resource.
     */
    override val identifier: String
        get() = getOrThrow("identifier") {
            name.lowerUnderscorize()
        }
    /**
     * The namespace of this rest_resource.
     */
    override val namespace: String
        get() = getOrThrow("namespace") {
            "${_model.retrieve<String>("project.namespace")}.resource.$identifier"
        }
    /**
     * The path of this rest_resource.
     */
    override val path: String
        get() = getOrThrow("path") {
            "/${identifier.lowerUnderscorize()}"
        }
    /**
     * The description of this rest_resource.
     */
    override val description: String
        get() = getOrThrow("description") {
            name
        }
    /**
     * operations
     */
    override val operations: List<RestOperation>
        = RestOperationRecord.from(getList("operations"), _model, this)
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(model: Model): RestResourceList {
            val entities = model.getList<Record>("rest_resources", emptyList()).map {
                RestResourceRecord(it.normalizeCamelcase(), model)
            }
            return RestResourceList(entities, model)
        }
    }
}