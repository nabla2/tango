package laplacian.arch.service.rest.model
import laplacian.metamodel.model.Entity
import laplacian.util.*
/**
 * A container for records of rest_resource
 */
class RestResourceList(
    list: List<RestResource>,
    val model: Model
) : List<RestResource> by list {
    val inNamespace: List<RestResource>
        get() = filter {
            it.namespace.startsWith(model.retrieve<String>("project.namespace")!!)
        }
}