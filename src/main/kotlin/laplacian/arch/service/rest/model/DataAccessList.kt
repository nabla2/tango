package laplacian.arch.service.rest.model
import laplacian.metamodel.model.Entity
import laplacian.util.*
/**
 * A container for records of data_access
 */
class DataAccessList(
    list: List<DataAccess>,
    val model: Model
) : List<DataAccess> by list {
    /**
     * contains_jooq_scripts
     */
    val containsJooqScripts: Boolean
        get() = any{ it.scriptType.contains("jooq") }
}