package laplacian.arch.record
import laplacian.arch.model.Client
import laplacian.arch.model.ClientList
import laplacian.arch.model.ClientFeature
import laplacian.arch.client.pwa.model.Feature
import laplacian.arch.client.pwa.record.FeatureRecord
import laplacian.arch.model.ServiceDependency
import laplacian.arch.service.model.Service
import laplacian.arch.service.record.ServiceRecord
import laplacian.util.*
/**
 * client
 */
data class ClientRecord (
    private val _record: Record,
    private val _model: Model
): Client, Record by _record {
    /**
     * The name of this client.
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * The identifier of this client.
     */
    override val identifier: String
        get() = getOrThrow("identifier") {
            name.lowerUnderscorize()
        }
    /**
     * The version of this client.
     */
    override val version: String
        get() = getOrThrow("version")
    /**
     * client_features
     */
    override val clientFeatures: List<ClientFeature>
        = ClientFeatureRecord.from(getList("client_features", emptyList()), _model, this)
    /**
     * service_dependencies
     */
    override val serviceDependencies: List<ServiceDependency>
        = ServiceDependencyRecord.from(getList("service_dependencies", emptyList()), _model, this)
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(model: Model): ClientList {
            val entities = model.getList<Record>("clients", emptyList()).map {
                ClientRecord(it.normalizeCamelcase(), model)
            }
            return ClientList(entities, model)
        }
    }
}