package laplacian.arch.record
import laplacian.arch.model.ClientFeature
import laplacian.arch.model.Client
import laplacian.arch.client.pwa.model.Feature
import laplacian.arch.client.pwa.record.FeatureRecord
import laplacian.util.*
/**
 * client_feature
 */
data class ClientFeatureRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the client which aggregates this client_feature
     */
    override val client: Client
): ClientFeature, Record by _record {
    /**
     * The feature_name of this client_feature.
     */
    override val featureName: String
        get() = getOrThrow("featureName")
    /**
     * feature
     */
    override val feature: Feature
        get() = FeatureRecord.from(_model).find {
            it.name == featureName
        } ?: throw IllegalStateException(
            "There is no feature which meets the following condition(s): "
            + "ClientFeature.feature_name == feature.name (=$featureName) "
            + "Possible values are: " + FeatureRecord.from(_model).map {
              "(${ it.name })"
            }.joinToString()
        )
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, client: Client) = records.map {
            ClientFeatureRecord(it.normalizeCamelcase(), model, client = client)
        }
    }
}