package laplacian.arch.record
import laplacian.arch.model.ServiceDependency
import laplacian.arch.model.Client
import laplacian.arch.service.model.Service
import laplacian.arch.service.record.ServiceRecord
import laplacian.util.*
/**
 * service_dependency
 */
data class ServiceDependencyRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the client which aggregates this service_dependency
     */
    override val client: Client
): ServiceDependency, Record by _record {
    /**
     * The service_name of this service_dependency.
     */
    override val serviceName: String
        get() = getOrThrow("serviceName")
    /**
     * service
     */
    override val service: Service
        get() = ServiceRecord.from(_model).find {
            it.name == serviceName
        } ?: throw IllegalStateException(
            "There is no service which meets the following condition(s): "
            + "ServiceDependency.service_name == service.name (=$serviceName) "
            + "Possible values are: " + ServiceRecord.from(_model).map {
              "(${ it.name })"
            }.joinToString()
        )
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, client: Client) = records.map {
            ServiceDependencyRecord(it.normalizeCamelcase(), model, client = client)
        }
    }
}