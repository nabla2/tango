package laplacian.arch.record
import laplacian.arch.model.GraphqlQuery
import laplacian.arch.model.GraphqlQueryList
import laplacian.metamodel.model.Entity
import laplacian.metamodel.record.EntityRecord
import laplacian.util.*
/**
 * graphql_query
 */
data class GraphqlQueryRecord (
    private val _record: Record,
    private val _model: Model
): GraphqlQuery, Record by _record {
    /**
     * The name of this graphql_query.
     */
    override val name: String
        get() = getOrThrow("name")
    /**
     * The identifier of this graphql_query.
     */
    override val identifier: String
        get() = getOrThrow("identifier") {
            name.lowerUnderscorize()
        }
    /**
     * The description of this graphql_query.
     */
    override val description: String
        get() = getOrThrow("description") {
            "$name query."
        }
    /**
     * The query of this graphql_query.
     */
    override val query: String
        get() = getOrThrow("query")
    /**
     * Defines this graphql_query is multiple or not.
     */
    override val multiple: Boolean
        get() = getOrThrow("multiple") {
            false
        }
    /**
     * The record_entity_name of this graphql_query.
     */
    override val recordEntityName: String? by _record
    /**
     * The record_type of this graphql_query.
     */
    override val recordType: String
        get() = getOrThrow("recordType") {
            recordEntity?.className ?: throw IllegalStateException(
                "It is needed to define record_type or record_entity_name."
            )
        }
    /**
     * The return_type of this graphql_query.
     */
    override val returnType: String
        get() = getOrThrow("returnType") {
            recordType + if (multiple) "[]" else ""
        }
    /**
     * record_entity
     */
    override val recordEntity: Entity?
        get() = EntityRecord.from(_model).find {
            it.name == recordEntityName
        }
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(model: Model): GraphqlQueryList {
            val entities = model.getList<Record>("graphql_queries", emptyList()).map {
                GraphqlQueryRecord(it.normalizeCamelcase(), model)
            }
            return GraphqlQueryList(entities, model)
        }
    }
}