package laplacian.tango
import laplacian.util.*
import laplacian.ModelLoader
import java.io.File
/**
 * A loader class for LaplacianTangoModel.
 */
class LaplacianTangoModelLoader : ModelLoader<LaplacianTangoModel> {
    /**
     * load a model object from the given yaml files.
     */
    override fun load(files: Iterable<File>): LaplacianTangoModel {
        val model: Model = YamlLoader.readObjects(files)
        return LaplacianTangoModel(model)
    }
}