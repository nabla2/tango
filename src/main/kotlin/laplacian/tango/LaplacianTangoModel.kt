package laplacian.tango
import laplacian.util.*
import laplacian.tango.model.DeckList
import laplacian.tango.record.DeckRecord
import laplacian.util.*
import laplacian.tango.model.CardList
import laplacian.tango.record.CardRecord
class LaplacianTangoModel(model: Model): Map<String, Any?> by model {
    val decks: DeckList = DeckRecord.from(model)
    val cards: CardList = CardRecord.from(model)
}