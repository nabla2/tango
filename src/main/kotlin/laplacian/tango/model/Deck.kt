package laplacian.tango.model
import laplacian.util.*
/**
 * deck
 */
interface Deck {
    /**
     * The title of this deck.
     */
    val title: String
    /**
     * card_entries
     */
    val cardEntries: List<CardEntry>
    /**
     * cards
     */
    val cards: List<Card>
        get() = cardEntries.map{ it.card }
}