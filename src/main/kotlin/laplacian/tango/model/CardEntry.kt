package laplacian.tango.model
import laplacian.util.*
/**
 * card_entry
 */
interface CardEntry {
    /**
     * The card_front_content of this card_entry.
     */
    val cardFrontContent: String
    /**
     * The score of this card_entry.
     */
    val score: Int
    /**
     * deck
     */
    val deck: Deck
    /**
     * card
     */
    val card: Card
}