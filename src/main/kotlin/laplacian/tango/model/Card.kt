package laplacian.tango.model
import laplacian.util.*
/**
 * card
 */
interface Card {
    /**
     * The front_content of this card.
     */
    val frontContent: String
    /**
     * The back_content of this card.
     */
    val backContent: String
}