package laplacian.tango.model
import laplacian.util.*
/**
 * A container for records of card
 */
class CardList(
    list: List<Card>,
    val model: Model
) : List<Card> by list {
}