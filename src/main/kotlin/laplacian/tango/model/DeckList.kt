package laplacian.tango.model
import laplacian.util.*
/**
 * A container for records of deck
 */
class DeckList(
    list: List<Deck>,
    val model: Model
) : List<Deck> by list {
}