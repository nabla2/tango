package laplacian.tango.record
import laplacian.tango.model.CardEntry
import laplacian.tango.model.Deck
import laplacian.tango.model.Card
import laplacian.util.*
/**
 * card_entry
 */
data class CardEntryRecord (
    private val _record: Record,
    private val _model: Model,
    /**
     * the deck which aggregates this card_entry
     */
    override val deck: Deck
): CardEntry, Record by _record {
    /**
     * The card_front_content of this card_entry.
     */
    override val cardFrontContent: String
        get() = getOrThrow("cardFrontContent")
    /**
     * The score of this card_entry.
     */
    override val score: Int
        get() = getOrThrow("score") {
            3
        }
    /**
     * card
     */
    override val card: Card
        get() = CardRecord.from(_model).find {
            it.frontContent == cardFrontContent
        } ?: throw IllegalStateException(
            "There is no card which meets the following condition(s): "
            + "CardEntry.card_front_content == card.front_content (=$cardFrontContent) "
            + "Possible values are: " + CardRecord.from(_model).map {
              "(${ it.frontContent })"
            }.joinToString()
        )
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(records: RecordList, model: Model, deck: Deck) = records.map {
            CardEntryRecord(it.normalizeCamelcase(), model, deck = deck)
        }
    }
}