package laplacian.tango.record
import laplacian.tango.model.Card
import laplacian.tango.model.CardList
import laplacian.util.*
/**
 * card
 */
data class CardRecord (
    private val _record: Record,
    private val _model: Model
): Card, Record by _record {
    /**
     * The front_content of this card.
     */
    override val frontContent: String
        get() = getOrThrow("frontContent")
    /**
     * The back_content of this card.
     */
    override val backContent: String
        get() = getOrThrow("backContent")
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(model: Model): CardList {
            val entities = model.getList<Record>("cards", emptyList()).map {
                CardRecord(it.normalizeCamelcase(), model)
            }
            return CardList(entities, model)
        }
    }
}