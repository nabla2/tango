package laplacian.tango.record
import laplacian.tango.model.Deck
import laplacian.tango.model.DeckList
import laplacian.tango.model.CardEntry
import laplacian.tango.model.Card
import laplacian.util.*
/**
 * deck
 */
data class DeckRecord (
    private val _record: Record,
    private val _model: Model
): Deck, Record by _record {
    /**
     * The title of this deck.
     */
    override val title: String
        get() = getOrThrow("title")
    /**
     * card_entries
     */
    override val cardEntries: List<CardEntry>
        = CardEntryRecord.from(getList("card_entries", emptyList()), _model, this)
    companion object {
        /**
         * creates record list from list of map
         */
        fun from(model: Model): DeckList {
            val entities = model.getList<Record>("decks", emptyList()).map {
                DeckRecord(it.normalizeCamelcase(), model)
            }
            return DeckList(entities, model)
        }
    }
}