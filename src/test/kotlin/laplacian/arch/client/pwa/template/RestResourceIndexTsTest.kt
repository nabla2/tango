package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class RestResourceIndexTsTest {

    val template = assertion.withTemplate(
        "template/client-pwa/src/components/api/{each services as service}{hyphen service.identifier}/index.ts.hbs"
    )

    @Test
    fun test_generates_index_file_of_all_resources_in_the_service() {
        template.assertSameContent(
            "target/client-pwa/src/components/api/flashcard/index.ts"
        ) { model ->
            mapOf("service" to model.services.find{ it.identifier == "flashcard" })
        }
    }
}
