package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class FeatureViewModelTsTest {

    val template = assertion.withTemplate(
        "template/client-pwa/src/components/features/{each features as feature}{hyphen feature.name}/store/view-model.ts.hbs"
    )

    @Test
    fun generate_view_model_class_of_the_flashcard_feature() {
        template.assertSameContent(
            "target/client-pwa/src/" +
            "components/features/" +
            "flashcard/store/view-model.ts"
        ) { m -> mapOf("feature" to m.features.find{ it.name == "flashcard" })}
    }
}
