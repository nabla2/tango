package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class FeatureReduxStoreIndexTsxTest {
    val template = assertion.withTemplate(
         "template/client-pwa/src/components/core/redux.ts.hbs"
    )

    @Test
    fun test_generating_store_bundle_for_a_feature() {
        template.assertSameContent(
            "target/client-pwa/src/components/core/redux.ts"
        ) { model ->
            val flashcardFeature = model.features.find{ it.name == "flashcard" }!!
            mapOf("feature" to flashcardFeature)
        }
    }
}
