package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class PageActionTsTest {
    val template = assertion.withTemplate(
        "template/client-pwa/src/components/" +
        "features/{each features as feature}{hyphen feature.name}/" +
        "pages/{each feature.pages as page}{hyphen page.name}/" +
        "actions/{each page.actions as action}" +
        "{hyphen action.name}-action.ts.hbs"
    )

    @Test
    fun it_generates_redux_action() {
        val toBeFile = "target/client-pwa/src/components/" +
                       "features/flashcard/" +
                       "pages/main/" +
                       "actions/" +
                       "flip-card-action.ts"
        template.assertSameContent(toBeFile) { model ->
             val flashcard = model.features.find{ it.name == "flashcard" }!!
             val mainPage = flashcard.pages.find{ it.name == "main" }!!
             val action = mainPage.actions.find{ it.name == "flip_card"}
             model + ("feature" to flashcard) +
                     ("page" to mainPage) +
                     ("action" to action)

        }
    }

    @Test
    fun it_generates_redux_action_with_payload() {
        val toBeFile = "target/client-pwa/src/components/" +
                       "features/flashcard/" +
                       "pages/main/" +
                       "actions/" +
                       "change-deck-action.ts"
        template.assertSameContent(toBeFile) { model ->
             val flashcard = model.features.find{ it.name == "flashcard" }!!
             val mainPage = flashcard.pages.find{ it.name == "main" }!!
             val action = mainPage.actions.find{ it.name == "change_deck"}!!
             mapOf(
                 "feature" to flashcard,
                 "page" to mainPage,
                 "action" to action
             )
        }
    }

    @Test
    fun it_generates_redux_action_including_rest_api_call() {
        template.assertSameContent(
            "target/client-pwa/src/components/features/flashcard/pages/main/actions/load-decks-action.ts"
        ) { model ->
             val flashcard = model.features.find{ it.name == "flashcard" }!!
             val mainPage = flashcard.pages.find{ it.name == "main" }!!
             val action = mainPage.actions.find{ it.name == "load_decks"}!!
             mapOf(
                 "feature" to flashcard,
                 "page" to mainPage,
                 "action" to action
             )
        }
    }
}
