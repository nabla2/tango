package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class PageActionIndexTsTest {
    val template = assertion.withTemplate(
        "template/client-pwa/src/components/" +
        "features/{each features as feature}{hyphen feature.name}/" +
        "pages/{each feature.pages as page}{hyphen page.name}/" +
        "actions/index.ts.hbs"
    )

    @Test
    fun it_generates_index_file_of_page_actions() {
        val toBeFile = "target/client-pwa/src/components/" +
                       "features/flashcard/" +
                       "pages/main/" +
                       "actions/" +
                       "index.ts"
        template.assertSameContent(toBeFile) { model ->
             val flashcard = model.features.find{ it.name == "flashcard" }!!
             val mainPage = flashcard.pages.find{ it.name == "main" }!!
             model + ("feature" to flashcard) +
                     ("page" to mainPage)

        }
    }
}
