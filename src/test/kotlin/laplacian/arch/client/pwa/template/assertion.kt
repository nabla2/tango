package laplacian.arch.client.pwa.template

import laplacian.arch.ArchModelTemplateAssertion

val assertion = ArchModelTemplateAssertion().withModel("model", "application/{*,**/*}.yml")
