package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class IndexHtmlTest {

    val indexHtml = assertion.withTemplate("template/client-pwa/src/index.html.hbs")

    @Test
    fun it_generates_html_file() {
        indexHtml.assertSameContent("target/client-pwa/src/index.html")
    }
}
