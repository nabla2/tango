package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class ManifestJsonTest {

    val manifiestFile = assertion.withTemplate("template/client-pwa/src/manifest.hbs.json")

    @Test
    fun it_generates_manifest_json_file() {
        manifiestFile.assertSameContent("target/client-pwa/src/manifest.json")
    }
}
