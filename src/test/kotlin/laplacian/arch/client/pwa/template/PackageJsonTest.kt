package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class PackageJsonTest {

    val packageJson = assertion.withTemplate(
        "template/client-pwa/package.hbs.json"
    )

    @Test
    fun it_generates_package_json_file() {
        packageJson.assertSameContent("target/client-pwa/package.json")
    }
}
