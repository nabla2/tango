package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class FeatureReducerTsTest {

    val template = assertion.withTemplate(
        "template/client-pwa/src/components/features/" +
        "{each features as feature}{hyphen feature.name}/" +
        "store/reducer.ts.hbs"
    )

    @Test
    fun generate_the_redux_store_class() {
        val toBeFile = "target/client-pwa/src/components/features/" +
                       "flashcard/store/" +
                       "reducer.ts"
        template.assertSameContent(toBeFile) { m ->
            m + ("feature" to m.features.find{ it.name == "flashcard" }!!)
        }
    }
}
