package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class RestResourceTsTest {

    val template = assertion.withTemplate(
        "template/client-pwa/src/components/api/{each services as service}{hyphen service.identifier}/{each service.resources as resource}{hyphen resource.identifier}-resource.ts.hbs"
    )

    @Test
    fun test_generates_rest_resource_client_interface() {
        template.assertSameContent(
            "target/client-pwa/src/components/api/flashcard/deck-resource.ts"
        ) { model ->
            val flashcardService = model.services.find{ it.name == "flashcard"}!!
            val deckResource = flashcardService.resources.find{ it.name == "deck" }
            mapOf(
                "service" to flashcardService,
                "resource" to deckResource
            )
        }
    }
}
