package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class PageTsxTest {

    val template = assertion.withTemplate(
        "template/client-pwa/src/components/features/{each features as feature}{hyphen feature.name}/pages/{each feature.pages as page}{hyphen page.name}/{hyphen page.name}-page.tsx.hbs"
    )

    @Test
    fun it_generates_the_entity_interface_of_entity() {
        val toBeFile = "target/client-pwa/src/components/" +
                       "features/flashcard/" +
                       "pages/main/" +
                       "main-page.tsx"
        template.assertSameContent(toBeFile) { model -> mapOf(
            "feature" to model.features.find{ it.name == "flashcard" }!!,
            "page" to model.pages.find{ it.name == "main" }!!
        )}
    }
}
