package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class EntityClassTsTest {

    @Test
    fun test_generating_index_file() {
        val templateForIndex = assertion.withTemplate(
            "template/client-pwa/src/components/" +
            "entities/" +
            "index.ts.hbs"
        )
        templateForIndex.assertSameContent(
            "target/client-pwa/src/components/entities/index.ts"
        )
    }

    val template = assertion.withTemplate(
        "template/client-pwa/src/components/entities/{each entities.in_namespace as entity}{if entity.top_level}{hyphen entity.name}.ts.hbs"
    )

    @Test
    fun test_generating_deck_entity() {
        template.assertSameContent("target/client-pwa/src/components/entities/deck.ts") { model ->
            model + ("entity" to model.entities.find{ it.name == "deck"}!!)
        }
    }

    @Test
    fun test_generating_card_entity() {
        template.assertSameContent("target/client-pwa/src/components/entities/card.ts") { model ->
            model + ("entity" to model.entities.find{ it.name == "card"}!!)
        }
    }
}

