package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class PageActionSpecTsTest {

    val template = assertion.withTemplate(
        "template/client-pwa/src/components/features/{each features as feature}{hyphen feature.name}/pages/{each feature.pages as page}{hyphen page.name}/actions/{each page.actions as action}{if action.examples}{hyphen action.name}-action.spec.ts.hbs"
    )

    @Test
    fun it_generates_the_entity_interface_of_entity() {
        val toBeFile = "target/client-pwa/src/components/" +
                       "features/flashcard/" +
                       "pages/main/" +
                       "actions/" +
                       "change-deck-action.spec.ts"
        template.assertSameContent(toBeFile) { model ->
             val flashcard = model.features.find{ it.name == "flashcard" }!!
             val mainPage = flashcard.pages.find{ it.name == "main" }!!
             val action = mainPage.actions.find{ it.name == "change_deck"}
             model + ("feature" to flashcard) +
                     ("page" to mainPage) +
                     ("action" to action)

        }
    }

}
