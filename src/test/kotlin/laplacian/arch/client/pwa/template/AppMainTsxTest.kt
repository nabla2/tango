package laplacian.arch.client.pwa.template

import org.junit.jupiter.api.Test

class AppMainTsxTest {

    val template = assertion.withTemplate(
        "template/client-pwa/src/components/app/app-main.tsx.hbs"
    )

    @Test
    fun test_generating_app_main_component() {
        template.assertSameContent(
            "target/client-pwa/src/components/app/app-main.tsx"
        )
    }
}
