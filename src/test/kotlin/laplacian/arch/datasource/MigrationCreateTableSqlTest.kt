package laplacian.arch.datasource

import org.junit.jupiter.api.Test

class MigrationCreateTableSqlTest {

    val template = assertion.withTemplate(
        "template/datasource/{each datasources as datasource}{hyphen datasource.identifier}/src/main/resources/db/migration/{each datasource.top_level_entities as top_level}V1.0.{@index}__create_{plural top_level.identifier}_table.sql.hbs"
    )

    @Test
    fun test_generate_create_table_statement_for_an_entity_which_does_not_have_any_aggregates() {
        template.assertSameContent(
            "target/datasource/flashcard-db/src/main/resources/db/migration/V1.0.0__create_cards_table.sql"
        ) { model ->
            mapOf(
                "datasource" to model.datasources.find{ it.name == "flashcard_db" },
                "top_level" to model.entities.find{ it.name == "card" }
            )
        }
    }


    @Test
    fun test_generate_create_table_statement_for_an_entity_with_aggregations() {
        template.assertSameContent(
            "target/datasource/flashcard-db/src/main/resources/db/migration/V1.0.1__create_decks_table.sql"
        ) { model ->
            mapOf(
                "datasource" to model.datasources.find{ it.name == "flashcard_db" },
                "top_level" to model.entities.find{ it.name == "deck" }
            )
        }
    }
}

