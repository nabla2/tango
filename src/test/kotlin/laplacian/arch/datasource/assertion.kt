package laplacian.arch.datasource

import laplacian.arch.ArchModelTemplateAssertion

val assertion = ArchModelTemplateAssertion().withModel("model", "application/{*,**/*}.yml")
