package laplacian.arch.service.rest.template

import laplacian.arch.ArchModelTemplateAssertion

val assertion = ArchModelTemplateAssertion().withModel("model", "application/{*,**/*}.yml")
