package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class ResouceInterfaceKtTest {

    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/kotlin/{each service.resources as resource}{path resource.namespace}/{upper-camel resource.identifier}Resource.kt.hbs"
    )

    @Test
    fun test_generates_rest_resource_interface() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/resource/deck/DeckResource.kt"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.identifier == "flashcard" },
                "resource" to model.rest_resources.find{ it.identifier == "deck" }
            )
        }
    }
}
