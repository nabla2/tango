package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class RootQueryResolverKtTest {

    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/kotlin/{path service.namespace}/query/QueryResolver.kt.hbs"
    )

    @Test
    fun test_it_generates_an_root_query_resolver_for_top_level_entities() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/service/flashcard/query/QueryResolver.kt"
        ) { model ->
            mapOf("service" to model.services.find{ it.name == "flashcard" }!!)
        }
    }
}
