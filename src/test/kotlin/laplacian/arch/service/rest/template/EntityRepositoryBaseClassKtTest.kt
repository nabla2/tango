package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class EntityRepositoryBaseClassKtTest {

    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/kotlin/{each service.relating_top_level_entities as top_level}{path top_level.namespace}/entity/{top_level.identifier}/{top_level.class_name}RepositoryBase.kt.hbs"
    )

    @Test
    fun test_generates_repository_implementation_for_the_entity_which_has_no_relationships() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/entity/card/CardRepositoryBase.kt"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" }!!,
                "top_level" to model.entities.find{ it.name == "card" }!!,
                "entity" to model.entities.find{ it.name == "card" }!!
            )
        }
    }

    @Test
    fun test_generates_repository_implementation_for_the_entity_with_relationships() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/entity/deck/DeckRepositoryBase.kt"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" }!!,
                "top_level" to model.entities.find{ it.name == "deck" }!!,
                "entity" to model.entities.find{ it.name == "deck" }!!
            )
        }
    }
}
