package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class RootQueryGraphqlTest {

    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/resources/{path service.namespace}/query/query.graphqls.hbs"
    )

    @Test
    fun test_it_generates_an_root_query_resolver_for_top_level_entities() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/resources/laplacian/tango/service/flashcard/query/Query.graphqls"
        ) { model ->
            mapOf("service" to model.services.find{ it.name == "flashcard" }!!)
        }
    }
}
