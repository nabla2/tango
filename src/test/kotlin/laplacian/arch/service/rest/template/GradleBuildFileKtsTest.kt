package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class GradleBuildFileKtsTest {
    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/build.gradle.kts.hbs"
    )
    @Test
    fun test_generates_build_file_for_each_service() {
        template.assertSameContent(
            "target/service-api/flashcard/build.gradle.kts"
        ) { model ->
            mapOf("service" to model.services.find{ it.name == "flashcard" })
        }
    }
}
