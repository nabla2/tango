package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class EntityResolverKtTest {

    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/kotlin/{each service.relating_top_level_entities as top_level}{path top_level.namespace}/entity/{top_level.identifier}/{each top_level.aggregated_entities as entity}{entity.class_name}Resolver.kt.hbs"
    )

    @Test
    fun test_generates_a_graphql_resolver_for_oplevel_entity_model() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/entity/card/CardResolver.kt"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" }!!,
                "top_level" to model.entities.find{ it.name == "card" }!!,
                "entity" to model.entities.find{ it.name == "card" }!!
            )
        }
    }

    @Test
    fun test_generates_a_graphql_resolver_for_entity_with_aggregate_relationship() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/entity/deck/DeckResolver.kt"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" }!!,
                "top_level" to model.entities.find{ it.name == "deck" }!!,
                "entity" to model.entities.find{ it.name == "deck" }!!
            )
        }
    }

    @Test
    fun test_generates_a_graphql_resolver_for_entity_which_has_a_relationships_to_another_entity() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/entity/deck/CardEntryResolver.kt"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" }!!,
                "top_level" to model.entities.find{ it.name == "deck" }!!,
                "entity" to model.entities.find{ it.name == "card_entry" }!!
            )
        }
    }
}
