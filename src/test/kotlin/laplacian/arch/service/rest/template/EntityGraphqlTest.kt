package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class EntityGraphqlTest {

    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/resources/{each service.relating_top_level_entities as top_level}{path top_level.namespace}/entity/{top_level.identifier}/{each top_level.aggregated_entities as entity}{entity.class_name}.graphqls.hbs"
    )

    @Test
    fun test_generates_graphql_schema_definition_for_a_toplevel_entity_model() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/resources/laplacian/tango/entity/card/Card.graphqls"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" }!!,
                "top_level" to model.entities.find{ it.name == "card" }!!,
                "entity" to model.entities.find{ it.name == "card" }!!
            )
        }
    }

    @Test
    fun test_generates_entity_with_aggregate_relationship() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/resources/laplacian/tango/entity/deck/Deck.graphqls"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" }!!,
                "top_level" to model.entities.find{ it.name == "deck" }!!,
                "entity" to model.entities.find{ it.name == "deck" }!!
            )
        }
    }

    @Test
    fun test_generates_entity_which_has_a_relationships_to_another_entity() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/resources/laplacian/tango/entity/deck/CardEntry.graphqls"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" }!!,
                "top_level" to model.entities.find{ it.name == "deck" }!!,
                "entity" to model.entities.find{ it.name == "card_entry" }!!
            )
        }
    }
}
