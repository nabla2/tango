package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class ApplicationPropertiesTest {

    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/resources/application.properties.hbs"
    )

    @Test
    fun test_generates_application_properties_for_this_service() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/resources/application.properties"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" }
            )
        }
    }
}
