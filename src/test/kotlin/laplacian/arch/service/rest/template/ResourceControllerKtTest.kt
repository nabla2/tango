package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class ResourceControllerKtTest {

    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/kotlin/{path service.namespace}/controller/{each service.resources as resource}{upper-camel resource.identifier}Controller.kt.hbs"
    )

    @Test
    fun test_generates_a_rest_controller_for_each_rest_resources() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/service/flashcard/controller/DeckController.kt"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" },
                "resource" to model.rest_resources.find{ it.name == "deck" }
            )
        }
    }
}
