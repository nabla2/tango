package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class GradleBuildSettingsKtsTest {
    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/settings.gradle.kts.hbs"
    )
    @Test
    fun test_generates_build_settings_property_file() {
        template.assertSameContent(
            "target/service-api/flashcard/settings.gradle.kts"
        ) { model ->
            mapOf("service" to model.services.find{ it.name == "flashcard" })
        }

    }
}
