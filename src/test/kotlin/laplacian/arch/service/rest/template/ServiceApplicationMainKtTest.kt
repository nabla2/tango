package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class ServiceApplicationMainKtTest {

    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/kotlin/{path service.namespace}/{upper-camel service.identifier}Application.kt.hbs"
    )

    @Test
    fun test_generates_service_application_main_class() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/service/flashcard/FlashcardApplication.kt"
        ) { model ->
            mapOf(
                "service" to model.services.find{ it.name == "flashcard" }
            )
        }
    }
}
