package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class GraphqlEndpointKtTest {
    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/kotlin/{path service.namespace}/query/Endpoint.kt.hbs"
    )

    @Test
    fun test_it_generates_an_endpoint_which_exposes_top_level_entities() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/service/flashcard/query/Endpoint.kt"
        ) { model ->
            mapOf("service" to model.services.find{ it.name == "flashcard" }!!)
        }
    }

}
