package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class ResourceImplKtTest {
    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/kotlin/{each service.resources as resource}{path resource.namespace}/{upper-camel resource.identifier}ResourceBase.kt.hbs"
    )

    @Test
    fun test_generates_an_implementation_for_a_rest_resource_interface() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/resource/deck/DeckResourceBase.kt"
        ) { model ->
             mapOf(
                "service" to model.services.find{ it.identifier == "flashcard" },
                "resource" to model.rest_resources.find{ it.identifier == "deck" }
            )
        }
    }
}
