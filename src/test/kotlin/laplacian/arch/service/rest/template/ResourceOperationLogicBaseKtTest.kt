package laplacian.arch.service.rest.template

import org.junit.jupiter.api.Test

class ResourceOperationLogicBaseKtTest {

    val template = assertion.withTemplate(
        "template/service-api/{each services as service}{lower-camel service.identifier}/src/gen/kotlin/{each service.resources as resource}{path resource.namespace}/{each resource.operations as operation}{lower-underscore operation.identifier}/{upper-camel operation.identifier}LogicBase.kt.hbs"
    )

    @Test
    fun test_generates_a_stub_implementation_of_the_logic_interface() {
        template.assertSameContent(
            "target/service-api/flashcard/src/gen/kotlin/laplacian/tango/resource/deck/get_decks/GetDecksLogicBase.kt"
        ) { model ->
            val service = model.services.find{ it.name == "flashcard" }!!
            val resource = model.rest_resources.find{ it.name == "deck" }!!
            val operation = resource.operations.find{ it.method == "GET" && it.path == "/"}!!
            mapOf(
                "service" to service,
                "resource" to resource,
                "operation" to operation
            )
        }
    }
}
