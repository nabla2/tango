import laplacian.gradle.task.ProjectTemplate
import laplacian.metamodel.MetamodelModelLoader
import laplacian.arch.ArchModelModelLoader
import laplacian.tango.LaplacianTangoModelLoader

group = "laplacian"
version = "1.0.0"

buildscript {
	repositories {
        maven {
			url = uri("../maven2/")
		}
        maven {
            url = uri("https://bitbucket.org/nabla2/maven2/raw/master/")
        }
	}
	dependencies {
        classpath("laplacian:laplacian-gradle-plugin:1.0.0")
        classpath("laplacian:tango-project:1.0.0")
	}
}

defaultTasks = listOf(
    "generateSystemModel",
    "generateHelmChart",
    "generateApplicationEntities",
    "generateClientPwa",
    "generateServiceApi",
	"generateDatasource",
    "loadDataset"
)

repositories {
    maven {
        url = uri("../maven2/")
    }
    maven {
        url = uri("https://bitbucket.org/nabla2/maven2/raw/master/")
    }
    jcenter()
}

plugins {
    kotlin("jvm") version "1.3.10"
    `maven-publish`
}

dependencies {
    compile(kotlin("stdlib"))
    compile(kotlin("reflect"))
    compile("laplacian:laplacian-gradle-plugin:1.0.0")
    testCompile("org.junit.jupiter:junit-jupiter-api:5.2.0")
    testCompile("org.junit.jupiter:junit-jupiter-engine:5.2.0")
}

val sourcesJar by tasks.creating(Jar::class.java) {
    classifier = "sources"
	from(sourceSets.main.get().allSource)
}

publishing {
    repositories {
        maven {
            url = uri("$projectDir/../maven2/")
        }
    }
    publications.create("mavenJava", MavenPublication::class.java) {
        from(components["java"])
        artifact(sourcesJar)
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

val generateSystemModel by tasks.creating(ProjectTemplate::class.java) {
    model {
        loader(MetamodelModelLoader())
        dir("model")
        include("system/global.yml", "system/client/*.yml", "system/service/*.yml", "metamodel/*.yml")
    }
    template {
        from("template/metamodel/src")
        into("src")
    }
    template {
        from("template/metamodel/schema")
        into("schema")
    }
    template {
        from("template/metamodel/vscode")
        into(".vscode")
    }
}

val generateHelmChart by tasks.creating(ProjectTemplate::class.java) {
    model {
        loader(ArchModelModelLoader())
        dir("model")
        include(
            "application/client/**/*.yml",
            "application/service/**/*.yml",
            "application/entity/*.yml",
            "application/datasource/**/*.yml",
            "application/global.yml",
            "metamodel/*.yml"
        )
    }
    template {
        from("template/helm-chart")
        into("target/helm-chart")
    }
}

val generateApplicationEntities by tasks.creating(ProjectTemplate::class.java) {
    model {
        loader(MetamodelModelLoader())
        dir("model/application")
        include("global.yml", "entity/*.yml")
    }
    template {
        from("template/metamodel/src")
        into("src")
    }
}

val generateClientPwa by tasks.creating(ProjectTemplate::class.java) {
    model {
        loader(ArchModelModelLoader())
        dir("model")
        include(
            "application/client/**/*.yml",
            "application/service/**/*.yml",
            "application/entity/*.yml",
            "application/global.yml",
            "metamodel/*.yml"
        )
    }
    template {
        from("template/client-pwa")
        into("target/client-pwa")
    }
}

val generateServiceApi by tasks.creating(ProjectTemplate::class.java) {
    model {
        loader(ArchModelModelLoader())
        dir("model")
        include(
            "application/service/**/*.yml",
            "application/datasource/**/*.yml",
            "application/entity/*.yml",
            "application/global.yml",
            "metamodel/*.yml"
        )
    }
    template {
        from("template/service-api")
        into("target/service-api")
    }
}

val generateDatasource by tasks.creating(ProjectTemplate::class.java) {
    model {
        loader(ArchModelModelLoader())
        dir("model")
        include(
            "application/datasource/**/*.yml",
            "application/entity/*.yml",
            "application/global.yml",
            "metamodel/*.yml"
        )
    }
    template {
        from("template/datasource")
        into("target/datasource")
    }
}

val loadDataset by tasks.creating(ProjectTemplate::class.java) {
    model {
        loader(LaplacianTangoModelLoader())
        dir("model/tango")
        include("*.js")
    }
    template {
        from("template/tango/client-pwa")
        into("target/client-pwa")
    }
    template {
        from("template/tango/datasource")
        into("target/datasource")
    }
}
