#!/usr/bin/env bash

set -e

helm install \
  --name nginx-ingress \
  --namespace kube-system \
  --set rbac.create=true \
  --set controller.service.loadBalancerIP=35.238.14.121 \
  stable/nginx-ingress

helm install \
  --name cert-manager \
  --namespace kube-system \
  --set ingressShim.defaultIssuerName=letsencrypt \
  --set ingressShim.defaultIssuerKind=Issuer \
  --set createCustomResource=false \
  stable/cert-manager

helm upgrade \
  --install \
  --namespace kube-system \
  cert-manager \
  --set createCustomResource=true \
  stable/cert-manager

